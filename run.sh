#!/bin/bash
../spark/bin/spark-submit \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.2.0 \
--class spark.ScalaKafkaWordCount \
--master local  \
./target/spark-kafka-word-count-0.1-SNAPSHOT.jar
