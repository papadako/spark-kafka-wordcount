package spark

import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.streaming.kafka010._
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.LongDeserializer

object ScalaKafkaWordCount {
  def main(args: Array[String]) {
    // Create context with 2 second batch interval
    val sparkConf = new SparkConf().setAppName("ScalaKafkaWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(2))

    // Create direct kafka stream with brokers and topics
    val topicsSet = "hy562-topic".split(",").toSet
    val kafkaParams = Map(
       "bootstrap.servers" -> "localhost:9092",
       "key.deserializer" -> classOf[LongDeserializer],
       "value.deserializer" -> classOf[StringDeserializer],
       "group.id" -> "Scala-Kafka-Word-Count",
       "auto.offset.reset" -> "earliest"
    )
    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams))

    // Get the lines, split them into words, count the words and print
    val lines = messages.map(_.value)
    val words = lines.flatMap(_.split(" "))
    val wordCounts = words.map(x => (x, 1L)).reduceByKey(_ + _)
    wordCounts.print()

    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }
}
